#ifndef UI1_H
#define UI1_H

#include <QtWidgets/QMainWindow>
#include "ui_qrcode.h"
#include "qrencode.h"
#include <vector>
#include <iostream>
#include <opencv\cv.h>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\highgui.hpp>
#include <opencv.hpp>

using namespace std;
using namespace cv;

struct myLine {
	QPoint startPnt;
	QPoint endPnt;
};

namespace Ui {
	class ui1Class;
};
class qrcode : public QMainWindow
{
	Q_OBJECT

public:
	qrcode(QWidget *parent = 0);
	~qrcode();
	QPoint startPnt;   //start point  
	QPoint endPnt;     //end point  
	bool isPressed;   //whether mouse's left-key is pressed
	cv::Mat img;
	void adjust2(int x, int y, float a);
	void adjust1(int x, int y, float a);
	float average(int x, int y);
	QImage qrcode::Mat2QImage(const Mat& mat);
	void drawblack(QPainter &painter, int scale_x, int scale_y, int x, int y);
	void drawwhite(QPainter &painter, int scale_x, int scale_y, int x, int y);
	void draw2(QPainter &painter, int width, int height);
	vector<myLine*> lines;

private:
	Ui::qrcodeClass ui;
	QRcode *qr;
	private slots:
	void save();
	bool Judge(QPoint *p);

	private slots:
	void generate();
	void reset();

protected:
	void paintEvent(QPaintEvent *event);
	void draw(QPainter &painter, int width, int height);
	void mousePressEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);

};

#endif // UI1_H
