/********************************************************************************
** Form generated from reading UI file 'qrcode.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QRCODE_H
#define UI_QRCODE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_qrcodeClass
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QTextEdit *textEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *qrcodeClass)
    {
        if (qrcodeClass->objectName().isEmpty())
            qrcodeClass->setObjectName(QStringLiteral("qrcodeClass"));
        qrcodeClass->resize(943, 623);
        centralWidget = new QWidget(qrcodeClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 420, 71, 61));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(430, 10, 410, 410));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(660, 440, 75, 23));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(440, 440, 75, 23));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(70, 440, 210, 31));
        qrcodeClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(qrcodeClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 943, 23));
        qrcodeClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(qrcodeClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        qrcodeClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(qrcodeClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        qrcodeClass->setStatusBar(statusBar);

        retranslateUi(qrcodeClass);

        QMetaObject::connectSlotsByName(qrcodeClass);
    } // setupUi

    void retranslateUi(QMainWindow *qrcodeClass)
    {
        qrcodeClass->setWindowTitle(QApplication::translate("qrcodeClass", "qrcode", 0));
        label->setText(QApplication::translate("qrcodeClass", "Data", 0));
        label_2->setText(QString());
        pushButton_2->setText(QApplication::translate("qrcodeClass", "reset", 0));
        pushButton->setText(QApplication::translate("qrcodeClass", "convert", 0));
    } // retranslateUi

};

namespace Ui {
    class qrcodeClass: public Ui_qrcodeClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QRCODE_H
